import './App.css';
import Speedtest from "./Components/Speedtest";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Speedtest/>
      </header>
    </div>
  );
}

export default App;
