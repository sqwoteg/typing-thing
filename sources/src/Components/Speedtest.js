import React, { Component } from "react";
import './Speedtest.css';
import WordsRaw from './SpeedtestWords.txt';

export default class Speedtest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timerNotStarted: true,
            timer: 60,
            userInput: "",
            currentWord: 0,
            correctSymbols: 0,
            misses: 0,
            wordsToType: [] // ["word", green, red, passed, missed]
        };
        fetch(WordsRaw)
            .then(r => r.text())
            .then(text => {
                const textArray = text.replace(/\.|,| - | – /gm, "").toLowerCase().split(' ').sort(() => Math.random() - 0.5)
                const wordsToType = [];
                for (let i = 0; i < textArray.length; i++) {
                    wordsToType.push([textArray[i], 0, 0, 0, 0])
                }
                let oldState = this.state
                oldState.wordsToType = wordsToType
                this.state = oldState
                this.forceUpdate()
            });
    }


    GetFirstMismatchedSymbol(sample, text) {
        for (var i = 0; i < (text.length > sample.length ? sample.length : text.length); i++) {
            if (sample[i] !== text[i]) return i
        }
        return text.length
    }

    InputChange(event) {
        if (this.state.timerNotStarted) {
            this.setState({timerNotStarted: false})

            let interval = setInterval(() => {
                if (this.state.timer < 2) clearInterval(interval)
                this.setState({
                    timer: (this.state.timer-1)
                });
            }, 1000);

        }
        if (!this.state.timer) return
        const newValue = event.target.value
        if (newValue.length < this.state.userInput.length) {
            let wordsToType = [...this.state.wordsToType], currentWord = this.state.currentWord
            let greens = this.GetFirstMismatchedSymbol(wordsToType[currentWord][0], newValue)
            let correctSymbols = this.state.correctSymbols - wordsToType[currentWord][1] + greens
            console.log(greens)
            wordsToType[currentWord][1] = greens
            wordsToType[currentWord][2] = newValue.length - greens
            this.setState({
                userInput: newValue,
                correctSymbols: correctSymbols,
                wordsToType: wordsToType
            })
        } else if (newValue[newValue.length - 1] === this.state.wordsToType[this.state.currentWord][0][newValue.length - 1] && this.state.wordsToType[this.state.currentWord][2] === 0) {

            let wordsToType = [...this.state.wordsToType], currentWord = this.state.currentWord
            wordsToType[currentWord][1]++


            this.setState({
                userInput: newValue,
                correctSymbols: this.state.correctSymbols + 1,
                wordsToType
            })
        } else {
            if (newValue[newValue.length - 1] === ' ' || newValue.length > this.state.wordsToType[this.state.currentWord][0].length + 1) {
                let wordsToType = [...this.state.wordsToType], currentWord = this.state.currentWord, misses = this.state.misses, correctSymbols = this.state.correctSymbols
                if (wordsToType[currentWord][0] + " " === newValue) {
                    wordsToType[currentWord][3]++;
                    correctSymbols += wordsToType[currentWord][0].length + 1 - wordsToType[currentWord][1]
                }
                else {
                    wordsToType[currentWord][4]++;
                    misses += wordsToType[currentWord][0].length - wordsToType[currentWord][2]
                }
                this.setState({
                    userInput: "",
                    misses: misses,
                    correctSymbols: correctSymbols,
                    currentWord: this.state.currentWord + 1
                })
            } else {
                let wordsToType = [...this.state.wordsToType], currentWord = this.state.currentWord
                wordsToType[currentWord][2]++

                this.setState({
                    userInput: newValue,
                    misses: this.state.misses + 1,
                    wordsToType
                })
            }
        }

    }

    render() {
        return (
            <div>
                <div className="words-to-type">
                    {this.state.wordsToType.slice(~~(this.state.currentWord / 10) * 10, (~~(this.state.currentWord / 10) + 1) * 10).map((word) => {
                        if (word[3])
                            return (<span className="green">{word[0]}</span>)
                        if (word[4])
                            return (<span className="red">{word[0]}</span>)

                        return (<span><span className="green">{word[0].slice(0, word[1])}</span><span className="red">{word[0].slice(word[1], word[1] + word[2])}</span>{word[0].slice(word[1] + word[2])}</span>)
                    })}
                    <br/>
                    {this.state.wordsToType.slice((~~(this.state.currentWord / 10) + 1) * 10, (~~(this.state.currentWord / 10) + 2) * 10).map((word) => {
                        return (<span>{word[0]}</span>)
                    })}
                </div>
                <input className={"textInput"} value={this.state.userInput} type="text" onChange={e => this.InputChange(e)}/>
                <br/>
                Time left: {this.state.timer} seconds / Misses: {this.state.misses} / Correct symbols: {this.state.correctSymbols} / Your speed: {~~(this.state.correctSymbols / (60 - this.state.timer) * 60) || 0} s/min
            </div>
        )
    }
}